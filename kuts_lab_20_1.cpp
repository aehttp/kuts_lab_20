/*
Filename: kuts_lab_20_1
Студент: Куць Ганна Олександрівна 
Группа: КН-1-3
Дата створення: 07.04.22
Лабораторна робота №20
Варіант 4
Тема: "«Алгоритмізація та програмування задач 
з використанням класів».

Мета роботи: Формування уміння розробляти алгоритмів та 
програмні коди для вирішення задач з використанням класів.

Умови завдання:
Перше: Обчислити та вивести кількість 
парних додатних елементів масиву а4(n).

Друге: У заданому масиві а8(n) знайти та вивести 
елементи й їх індекси, що менше заданого числа.

Третє: Відсортувати усі додатні елементи масив за зростання. 
Використовувати сортування обміну. 
*/
#include <iostream>
#include <ctime>
#include <cmath>
#include <windows.h>
#include <time.h>
#include <stdlib.h>
using namespace std;

class mas
{
	public:
	int n;
	
	int indices(); // Для роботи з індексами
	int quantity(); // Кількість додатніх, парних елементів
	int sort(); // Робота з сортуванням для квадратної матриці
	
	
	void random(); 
	void random1();
	
	void input();
	void input1();
	
	void print();
	void print1();                               
  	
  	private:
  	int a[100];
  	int b[100];
		
};

	void mas::random()
	{
		srand(time(NULL));
		for(int i=0; i<n; i++)
		{
			a[i] = rand()%100-50;
		}
	}
	
	void mas::print()
	{
		for(int i=0; i<n; i++)
		{
			printf("%4d", a[i]);
		}
		cout << endl;
	}
	
	void mas::input()
	{
  		cout << "Введіть кількість елементів: ";
  		for(int i = 0; i < n; i++)
  		cin >> a[i];
	}
	
	void mas::random1()
	{
		srand(time(NULL));
		for(int i=0; i<n; i++)
		{
			b[i] = rand()%100;
		}
	}
	
	void mas::print1()
	{
		for(int i=0; i<n; i++)
		{
			printf("%4d", b[i]);
		}
		cout << endl;
	}
	
	void mas::input1()
	{
  		cout << "Введіть кількість елементів: ";
  		for(int i=0; i<n; i++)
  		cin >> b[i];
	}
	
	int mas::quantity()
	{
		int count = 0;
		for(int i=0; i<n; i++)
      	{
        if(a[i]%2==0 && a[i]>=0)
        {
        	count++;
        }     
    }
    cout << count << endl;
   	return 0;
	}
	
	int mas::indices()
	{
		int x;
		cout << "N =";
		cin >> x;
		for (int i = 0; i < n; i++)
		{
			if (a[i]<x) 
			cout << i << " Елементи масива та їх індекси, які меньше x = "<< a[i] << endl;
		}
		return 0;
	}
	
	int mas::sort()
	{	
		for(int i=0; i<n; i++)
		{
			for(int j=0; j<n-1; j++)
			{
				if(b[j+1]>=0 && b[j]>=0 && b[j+1]<b[j])
				{
					int f = b[j];
					b[j] = b[j+1];
					b[j+1] = f;
				}
			}
		}
		cout<<endl;
		printf("Відсортований масив b(%d):\n",n);	
		for(int i=0; i<n; i++)
		{
			printf("%4d", b[i]);
		}
		cout << endl;
	}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	mas massive;
	
	cout << " Завдання №1 " << endl;
	cout << "Введіть кількість елементів: ";
	cin >> massive.n;
	massive.random(); 	
//	massive.input(); 
	massive.print();
	cout << "Количество четных, положительных элементов массивa: ";
	massive.quantity();
	cout << "______________________________________________________";
	cout << endl;
	cout << endl;
	
	cout << " Завдання №2 " << endl;
	massive.print();
	massive.indices();
	cout << "______________________________________________________";
	cout << endl;
	
	cout << endl;
	cout << " Завдання №3 " << endl;
	cout << "Введіть кількість елементів масиву: ";
	cin >> massive.n;
	massive.random1(); 
//	massive.input1(); 	
	massive.print1();
	massive.sort();
	
	system("pause");
	return 0;
}
